#ifndef PROSJEKT_BAD_RACOON_CORE_H
#define PROSJEKT_BAD_RACOON_CORE_H
#include <SFML/Graphics.hpp>
#include <sstream>
#include "Player.h"
#include "Collider.h"
#include "AudioController.h"

class Core {
public:

    Core();

    ~Core();
    void start();

    sf::RectangleShape RandomBox(sf::Vector2f size, sf::Vector2f position);



private:
    sf::RenderWindow _Window;
    sf::View _Camera;

    Player _player;
    Collider _collider;
    AudioController _AudioController;


    static const int WIDTH = 1280;
    static const int HEIGHT = 720;
    static const int MAX_FRAMERATE = 100;

    void run();
    void stop();
    bool isRunning;
};


#endif //PROSJEKT_BAD_RACOON_CORE_H
