#include "Core.h"

Core::Core() {
    isRunning = false;

}

Core::~Core() {

}

void Core::start() {
    if (isRunning) return;
    _Window.create(sf::VideoMode(WIDTH, HEIGHT), "Vidya game");
    _Window.setFramerateLimit(MAX_FRAMERATE);
    _Camera.setSize(_Window.getSize().x,_Window.getSize().y);

    run();
}
void Core::stop()
{
    if (!isRunning) return;

    isRunning = false;
}

void Core::run() {

    isRunning = true;
    sf::Clock clock;

    //region TEST STUFF REMOVE FOR REAL GAME
    _AudioController.setBackgroundMusic("res/sound/background_music/notstolen.ogg");sf::RectangleShape boxes[4];
    sf::RectangleShape floor = RandomBox(sf::Vector2f(1300.f,64.f), sf::Vector2f(650.f,700.f));
    floor.setFillColor(sf::Color::Yellow);
    boxes[0] = RandomBox(sf::Vector2f(64.f,64.f),sf::Vector2f(50.f,450.f));
    for(int i = 1; i <4;i = i+1){
        boxes[i] = RandomBox(sf::Vector2f(64.f,64.f),sf::Vector2f(boxes[i-1].getPosition().x + 300,450.f));
    };
    _player.getHitbox().setFillColor(sf::Color(0,0,255,128));
    _player.setPosition(0.f + (_player.getSprite().getGlobalBounds().width /2.f),0.f + (_player.getSprite().getGlobalBounds().height /2.f));

    //endregion

    _AudioController.playBackgroundMusic();
    while (isRunning)
    {
        sf::Time deltatime = clock.restart();

        sf::Event event;
        while (_Window.pollEvent(event))
        {
            switch (event.type)
            {
                //Case for closing _Window
                case sf::Event::Closed:
                    _Window.close();
                    isRunning = false;
                    break;
            }
        }

        //region Velocity for player. Move to own function later? for better input handling
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::A)){
            _player.updateVelocity(-5.0f, 0.0f);
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D)){
            _player.updateVelocity(5.0f, 0.0f);
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::W))
        {
            _player.updateVelocity(0.0f, -1300.0f);
            _player.setIsJumping(true);
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S)){
            _player.updateVelocity(0.0f, 5.0f);
        } else
        {
            _player.updateVelocity();
        }
        //endregion

        _player.movePosition(deltatime.asSeconds());


        //TODO: Collision on bottom and left side overlap while top and right stop before collision
        for(int i = 0; i < 4;i = i+1) {
        _collider.collisionTest(_player, boxes[i]);
        }
        _collider.collisionTest(_player, floor);

        _Camera.setCenter(_player.getPosition());

        _Window.clear(sf::Color::Cyan);

        _Window.setView(_Camera);

        _Window.draw(_player.getSprite());

        //TODO:REMOVE FOR REAL GAME AND SEEING HITBOX IS NOT NEEDED
        //_Window.draw(_player.getHitbox());
        //DRAW HITBOX FOR VISUAL CHECK OF COLLISION

        //TODO: REMOVE FOR REAL LEVEL CURRENTLY TEST BLOCKS
        for(int i = 0; i < 4;i = i+1) {
            _Window.draw(boxes[i]);
        }
        _Window.draw(floor);


        _Window.display();
    }

    stop();
}

//

sf::RectangleShape Core::RandomBox(sf::Vector2f size, sf::Vector2f position) {
    sf::RectangleShape collisiontestblox;
    collisiontestblox.setSize(size);
    collisiontestblox.setOrigin(size/2.f);
    collisiontestblox.setPosition(position);
    collisiontestblox.setFillColor(sf::Color::Red);

    return collisiontestblox;
}
