#ifndef PROSJEKT_BAD_RACOON_AUDIOCONTROLLER_H
#define PROSJEKT_BAD_RACOON_AUDIOCONTROLLER_H
#include <SFML/Audio.hpp>


class AudioController {
public:
    AudioController();
    void setBackgroundMusic(std::string path);
    void playBackgroundMusic();
    void pauseBackgroundMusic(){BackgroundMusic.pause();};

    void playSoundEffect(sf::SoundBuffer buffer);

    void StopAllSound();
    sf::Music BackgroundMusic;

private:
    //Music = Big files not loaded into ram

    //Sound = Smaller files loaded into ram
    sf::Sound SoundEffect;
};


#endif //PROSJEKT_BAD_RACOON_AUDIOCONTROLLER_H
