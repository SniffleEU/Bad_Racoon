#include "Texture.h"

sf::Texture& Texture::getTexture() {
    return texture;
}

Texture::Texture(std::string path) {
    if (!texture.loadFromFile(path))
    {
        printf("Failed to load image file to texture");
    }
}