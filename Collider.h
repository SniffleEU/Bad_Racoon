#ifndef PROSJEKT_BAD_RACOON_COLLIDER_H
#define PROSJEKT_BAD_RACOON_COLLIDER_H

#include <SFML/Graphics.hpp>
#include "Player.h"

class Collider {
public:
    Collider();

    void collisionTest(Player& player, sf::RectangleShape& rec2, float push = 0.0f);

private:
    bool AABBCollision(sf::RectangleShape& rec1, sf::RectangleShape& rec2, float& intersectX,float& intersectY,float& deltaX,float& deltaY );
    float SweptAABB(Player& player, sf::RectangleShape& rec2, float& normalX, float& normalY);
    bool SATCollision(sf::RectangleShape rec1, sf::RectangleShape rec2);
};


#endif //PROSJEKT_BAD_RACOON_COLLIDER_H
