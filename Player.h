#ifndef PROSJEKT_BAD_RACOON_PLAYER_H
#define PROSJEKT_BAD_RACOON_PLAYER_H
#include <SFML/Graphics.hpp>
#include "Texture.h"

class Player {
public:
    Player();
    void setPosition(float x, float y);
    void movePosition(float deltatime);

    sf::Sprite&  getSprite(){return PlayerSprite;};
    sf::Vector2f getPosition(){return PlayerSprite.getPosition();};
    sf::RectangleShape& getHitbox();
    void updateVelocity(float x = 0, float y = 0);
    sf::Vector2f getPlayerVelocity(){return PlayerVelocity;};
    void collisionChangeVelocity(float x, float y){PlayerVelocity.x += x; PlayerVelocity.y += y; };
    void playerMoveCollision(float x, float y);
    void setIsJumping(bool isJumping){Player::isJumping = isJumping; };

private:
    sf::Sprite PlayerSprite;
    //TODO: Animation for walking, jumping, dying osv.
    Texture PlayerTexture = Texture("res/tex/Racoon.png");

    sf::RectangleShape hitbox;

    sf::Vector2f PlayerVelocity;

    bool isJumping;
    float gravity = 450.f;
    float fallspeed = 10.f;

    float airdrag = 3.f;
    float maxMovementSpeed = 500.0f;

    void moveHitbox();
};


#endif //PROSJEKT_BAD_RACOON_PLAYER_H
