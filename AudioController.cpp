#include "AudioController.h"

void AudioController::setBackgroundMusic(std::string path) {
    if (!BackgroundMusic.openFromFile(path)){
        printf("Could not load music file");
    }
    BackgroundMusic.setLoop(true);
}

void AudioController::playBackgroundMusic() {
        BackgroundMusic.play();
}

AudioController::AudioController() {
    BackgroundMusic.setVolume(5.f);
    BackgroundMusic.setLoop(true);

}

void AudioController::playSoundEffect(sf::SoundBuffer buffer) {

    SoundEffect.setBuffer(buffer);
    SoundEffect.play();
}

void AudioController::StopAllSound() {
    BackgroundMusic.stop();
    SoundEffect.stop();
}