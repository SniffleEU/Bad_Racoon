#include "Player.h"

Player::Player() {
    //Sprite
    PlayerSprite.setTexture(PlayerTexture.getTexture());
    PlayerSprite.setOrigin(sf::Vector2f(PlayerSprite.getGlobalBounds().width, PlayerSprite.getGlobalBounds().height) / 2.0f);
    //Hitbox
    //TODO: Adjust size to match texture
    hitbox.setSize(sf::Vector2f(PlayerSprite.getGlobalBounds().width, PlayerSprite.getGlobalBounds().height));
    moveHitbox();
    hitbox.setOrigin(hitbox.getSize()/2.f);
}

void Player::setPosition(float x, float y) {
    PlayerSprite.setPosition(x,y);
}

void Player::movePosition(float deltatime) {
    PlayerSprite.move((PlayerVelocity.x * deltatime),PlayerVelocity.y * deltatime);


    //TODO: Fine tune "air drag"d
    if (PlayerVelocity.x != 0){
        if(PlayerVelocity.x < 0){
            PlayerVelocity.x += airdrag;
        }
        else
        {
            PlayerVelocity.x -= airdrag;
        }

    }
    //TODO: Fine tune gravity
    if(PlayerVelocity.y < gravity){PlayerVelocity.y += fallspeed;}
    moveHitbox();
}

sf::RectangleShape& Player::getHitbox() {
    moveHitbox();
    return hitbox;
}

void Player::updateVelocity(float x, float y) {
    //X movement / running
    if((PlayerVelocity.x + x) <= maxMovementSpeed
       && (PlayerVelocity.x + x) >= (maxMovementSpeed * -1))
        PlayerVelocity.x += x;

    //Y movement / jumping
    //TODO: Make it a jump instead of normal movement on the Y axis
   //if((PlayerVelocity.y + y) <= maxMovementSpeed
   //   && (PlayerVelocity.y + y) >= (maxMovementSpeed * -1))
   //    PlayerVelocity.y += y;
    //TODO: Only jump from while in collision with floor/platform on lowe x axis
    //TODO: Also tune jump amount you little shit
    if (!isJumping && PlayerVelocity.y > 0){PlayerVelocity.y += y;};


}

void Player::playerMoveCollision(float x, float y) {
    PlayerSprite.move(x,y);
    moveHitbox();
}

void Player::moveHitbox() {
    hitbox.setPosition(PlayerSprite.getPosition());
}
