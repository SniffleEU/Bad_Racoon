#include "Collider.h"

Collider::Collider() {
}

void Collider::collisionTest(Player &player, sf::RectangleShape &rec2, float push) {
    float intersectX;
    float intersectY;
    float deltaX;
    float deltaY;

    if(AABBCollision(player.getHitbox(),rec2, intersectX, intersectY, deltaX, deltaY))
    {
        push = std::min(std::max(push,0.0f), 1.0f);

        if(intersectX > intersectY){
            if (deltaX > 0.0f){
                player.playerMoveCollision(intersectX * (1.0f - push), 0.0f);
                rec2.move(-intersectX * push, 0.0f);
            }else
            {
                player.playerMoveCollision(-intersectX * (1.0f - push), 0.0f);
                rec2.move(intersectX * push, 0.0f);
            }
        }else{
            if (deltaY > 0.0f){
                player.playerMoveCollision(0.0f,intersectY * (1.0f - push));
                rec2.move(-intersectY * push, 0.0f);
                player.setIsJumping(false);

            }else
            {
                player.playerMoveCollision(0.0f,-intersectY * (1.0f - push));
                rec2.move(0.0f, intersectY * push);

            }
        }

    };

}

bool Collider::AABBCollision(sf::RectangleShape& rec1, sf::RectangleShape& rec2, float& intersectX,float& intersectY,float& deltaX,float& deltaY) {
    sf::Vector2f rec1Position = rec1.getPosition();
    sf::Vector2f rec1HalfSize = rec1.getSize() * 0.5f;
    sf::Vector2f rec2Position = rec2.getPosition();
    sf::Vector2f rec2HalfSize = rec2.getSize() * 0.5f;

    deltaX = rec2Position.x - rec1Position.x;
    deltaY = rec2Position.y - rec1Position.y;
    if(deltaX < 0)
    {
        intersectX = (deltaX * -1) - (rec2HalfSize.x + rec1HalfSize.x);
    }else
    {
        intersectX = deltaX - (rec2HalfSize.x + rec1HalfSize.x);
    }

    if(deltaY < 0)
    {
        intersectY = (deltaY * -1) - (rec2HalfSize.y + rec1HalfSize.y);
    }else
    {
        intersectY = deltaY - (rec2HalfSize.y + rec1HalfSize.y);
    }


    if (intersectX < 0.0f && intersectY < 0.0f){
        return true;
    }
    return false;
}

//region Swept AABB Replaced. Kanskje prøve igjen dersom det er nok tid før innlevering
float Collider::SweptAABB(Player &player, sf::RectangleShape& rec2, float &normalX, float &normalY) {
    float xInvEntry, yInvEntry;
    float xInvExit, yInvExit;

    if (player.getPlayerVelocity().x > 0.0f) {
        xInvEntry = rec2.getPosition().x - (player.getHitbox().getPosition().x + player.getHitbox().getSize().x);
        xInvExit = (rec2.getPosition().x + rec2.getSize().x) - player.getHitbox().getPosition().x;
    } else {
        xInvEntry = (rec2.getPosition().x + rec2.getSize().y) - player.getPosition().x;
        xInvExit = rec2.getPosition().x - (player.getHitbox().getPosition().x + player.getHitbox().getSize().x);
    }
    if (player.getPlayerVelocity().y > 0.0f) {
        yInvEntry = rec2.getPosition().y - (player.getHitbox().getPosition().y + player.getHitbox().getSize().y);
        yInvExit = (rec2.getPosition().y + rec2.getSize().y) - player.getHitbox().getPosition().y;
    } else {
        yInvEntry = (rec2.getPosition().y + rec2.getSize().y) - player.getHitbox().getPosition().y;
        yInvExit = rec2.getPosition().y - (player.getHitbox().getPosition().y + player.getHitbox().getSize().y);
    }

    float xEntry, yEntry;
    float xExit, yExit;
    if (player.getPlayerVelocity().x == 0.0f) {
        xEntry = -std::numeric_limits<float>::infinity();
        xExit = std::numeric_limits<float>::infinity();
    } else {
        xEntry = xInvEntry / player.getPlayerVelocity().x;
        xExit = xInvExit / player.getPlayerVelocity().x;
    }
    if (player.getPlayerVelocity().y == 0.0f) {
        yEntry = -std::numeric_limits<float>::infinity();
        yExit = std::numeric_limits<float>::infinity();
    } else {
        yEntry = yInvEntry / player.getPlayerVelocity().y;
        yExit = yInvExit / player.getPlayerVelocity().y;
    }

    float entryTime = std::max(xEntry, yEntry);
    float exitTime = std::min(xExit, yExit);

    if (entryTime > exitTime || xEntry < 0.0f && yEntry < 0.0f || xEntry > 1.0f || yEntry > 1.0f)
        //No collision:
    {
        normalX = 0.0f;
        normalY = 0.0f;
        return 1.0f;
    }
        //Collision:
    else {
        if (xEntry > yEntry) {
            if (xInvEntry < 0.0f) {
                normalX = 1.0f;
                normalY = 0.0f;
            } else {
                normalX = -1.0f;
                normalY = 0.0f;
            }
        } else {
            if (yInvEntry < 0.0f) {
                normalX = 0.0f;
                normalY = 1.0f;
            } else {
                normalX = 0.0f;
                normalY = -1.0f;
            }
        }
        return entryTime;
    }

}
//endregion


//region SAT collision, hvis tid bytt til SAT over AABB
bool Collider::SATCollision(sf::RectangleShape rec1, sf::RectangleShape rec2) {
    float length = rec2.getSize().x - rec1.getSize().x;
    float halfWidthRec1 = rec1.getSize().x * 0.5;
    float halfWidthRec2 = rec2.getSize().x * 0.5;

    float distanceBetweenRecs = length - halfWidthRec1 - halfWidthRec2;



    return false;
}
//endregion
