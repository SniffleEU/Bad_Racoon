#ifndef PROSJEKT_BAD_RACOON_TEXTURE_H
#define PROSJEKT_BAD_RACOON_TEXTURE_H

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

class Texture {
public:
    Texture(std::string path);
    sf::Texture& getTexture();

private:
    sf::Texture texture;

};


#endif //PROSJEKT_BAD_RACOON_TEXTURE_H
